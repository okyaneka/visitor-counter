var btn_home = $("#button-home"),
	btn_checkin = $("#button-checkin"),
	member = $("#member"),
	non_member = $("#non-member"),
	checked = $("#checked"),
	confirm = $(".confirm"),
	survey = $(".survey"),
	caption = $("#caption"),
	page_caption = $("#pagecaption"),
	logo = $("#logo"),
	backbutton = $("#backbutton"),
	notification = $("#notification");

function clear_form_content() {
	$("#member [name='nip']").val("");
	$("#member [name='keperluan']").val("");
	$("#non-member [name='nama']").val("");
	$("#non-member [name='institusi']").val("");
	$("#non-member [name='keperluan']").val("");
	$("#non-member [name='no-hp']").val("");
	$("#non-member [name='email']").val("");
	$("#non-member [name='alamat']").val("");
}

function back(){
	var capt = caption.text();
	caption.text("Presensi Perpustakaan");
	if (capt != caption.text()) {
		// alert("Changed");
		page_caption.text(capt);
	}

  clear_form_content();

	caption.css("top",90);
	page_caption.css("top",0);
	TweenLite.to(backbutton,1,{css:{left:830, rotation:0}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:-90}, ease:Power2.easeOut});
	TweenLite.to(caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(btn_checkin,1.5,{css:{left:750}, ease:Power2.easeOut});	
	TweenLite.to(checked,1.5,{css:{left:750}, ease:Power2.easeOut});	
	TweenLite.to(member,1.5,{css:{left:750}, ease:Power2.easeOut});	
	TweenLite.to(non_member,1.5,{css:{left:750}, ease:Power2.easeOut});	
	TweenLite.to(survey,1.5,{css:{left:750}, ease:Power2.easeOut});	
	TweenLite.to(confirm,1.5,{css:{left:750}, ease:Power2.easeOut});	
  
  $(".form-control-feedback").hide();
}

$(window).load(function(){
	TweenLite.to(caption,2,{css:{top:0},delay:1, ease:Power2.easeOut});
	TweenLite.to(logo,2,{css:{top:0},delay:1, ease:Power2.easeOut});
	TweenLite.to($("#maskot"),2,{css:{left:20},delay:1.5, ease:Power2.easeOut});  
	TweenLite.to(btn_home,2,{css:{left:0},delay:2, ease:Power2.easeOut});

	$("#checked table").DataTable({
    "lengthMenu": [[15], [15]],
    "lengthChange": false,
    "bInfo" : false,
    "oLanguage": { "sSearch": "" }
  });

  $(".dataTables_filter input").addClass("form-control col-md-8");
  $(".dataTables_filter input").attr("placeholder","Cari");

	$('#member').bootstrapValidator({
		message: '',feedbackIcons: {
			valid: 'glyphicon glyphicon-ok',
			invalid: 'glyphicon glyphicon-remove',
				validating: 'glyphicon glyphicon-refresh'
		},fields: {
			'nip': {container: '#nip',validators: {notEmpty: {message: ''}}},
			'keperluan': {container: '#keperluan',validators: {notEmpty: {message: ''}}},
			'no-hp': {container: '#no-hp',validators: {notEmpty: {message: ''}}},
			'email': {container: '#email',validators: {notEmpty: {message: ''}}},
			'alamat': {container: '#alamat',validators: {notEmpty: {message: ''}}}
		}
		}).on('success.form.bv', function(e) {
		// Prevent form submission
		  e.preventDefault();

		  // Get the form instance
		  var $form = $(e.target);

		  // Get the BootstrapValidator instance
		  var bv = $form.data('bootstrapValidator');

		  // Use Ajax to submit form data
		  $.post($form.attr('action'), $form.serialize(), function(result) {
		      console.log(result);
		  }, 'json');
    });

  	$('#non-member').bootstrapValidator({
    	message: '',feedbackIcons: {
    		valid: 'glyphicon glyphicon-ok',
    		invalid: 'glyphicon glyphicon-remove',
      		validating: 'glyphicon glyphicon-refresh'
    	},fields: {
    		'nama': {container: '#nama',validators: {notEmpty: {message: ''}}},
    		'institusi': {container: '#institusi',validators: {notEmpty: {message: ''}}},
      	'keperluan': {container: '#keperluan',validators: {notEmpty: {message: ''}}},
      	'no-hp': {container: '#no-hp',validators: {notEmpty: {message: ''}}},
    		'email': {container: '#email',validators: {notEmpty: {message: ''}}},
    		'alamat': {container: '#alamat',validators: {notEmpty: {message: ''}}}
    	}
  	}).on('success.form.bv', function(e) {
  		// Prevent form submission
        e.preventDefault();

        // Get the form instance
        var $form = $(e.target);

        // Get the BootstrapValidator instance
        var bv = $form.data('bootstrapValidator');

        // Use Ajax to submit form data
        $.post($form.attr('action'), $form.serialize(), function(result) {
            console.log(result);
        }, 'json');
    });
});

backbutton.click(function() {
	back();
}); 

$(".button.check-in").click(function() {
	page_caption.text("Check in");
	TweenLite.to(backbutton,1,{css:{left:650, rotation:-360}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(caption,1,{css:{top:90}, ease:Power2.easeOut});
	TweenLite.to(btn_checkin,1.5,{css:{left:0}, ease:Power2.easeOut});
});

$(".button.check-out").click(function() {
	page_caption.text("Check Out");
	TweenLite.to(backbutton,1,{css:{left:650, rotation:-360}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(caption,1,{css:{top:90}, ease:Power2.easeOut});
	TweenLite.to(checked,1.5,{css:{left:0}, ease:Power2.easeOut});
});

$(".button.member").click(function() {
	caption.text("Member Check In");
	TweenLite.to(caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:-90}, ease:Power2.easeOut});
	TweenLite.to(member,1.5,{css:{left:0}, ease:Power2.easeOut});
});

$(".button.non-member").click(function() {
	caption.text("Non Member Check In");
	TweenLite.to(caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:-90}, ease:Power2.easeOut});
	TweenLite.to(non_member,1.5,{css:{left:0}, ease:Power2.easeOut});
});

$("form").submit(function() {
	if ($(this).attr("id") == "non-member") {
    if ( $("#non-member #nama").val() == '' || $("#non-member #institusi").val() == '' || $("#non-member #keperluan").val() == '' || $("#non-member #no-hp").val() == '' || $("#non-member #email").val() == '' || $("#non-member #alamat").val() == '') {
      return;
    }
  } else {
    if ($("#member #nip").val() == '' || $("#member #keperluan").val() == '' || $("#member #no-hp").val() == '' || $("#member #email").val() == '' || $("#member #alamat").val() == '') {
      return;
    }
  }

  notification.show();
  var tl = new TimelineLite();
  tl.to(notification,0.25,{css:{opacity:1}, ease:Power2.easeIn})
  	.to(notification,3.25,{css:{opacity:0}, ease:Power2.easeIn});
  setTimeout(function() {
  	notification.hide();
  },3500);

  clear_form_content();

  $(".form-control-feedback").hide();

	page_caption.text("Check Out");
	TweenLite.to(caption,1,{css:{top:90}, ease:Power2.easeOut});
	TweenLite.to(page_caption,1,{css:{top:0}, ease:Power2.easeOut});
	TweenLite.to(checked,1.5,{css:{left:0}, ease:Power2.easeOut});
});

$(".info").click(function() {
	TweenLite.to(confirm,1,{css:{left:0}, ease:Power2.easeOut});
});

$(".cancel").click(function() {
	TweenLite.to(confirm,1,{css:{left:750}, ease:Power2.easeIn});
});

$(".accept").click(function() {
	TweenLite.to(survey,1,{css:{left:0}, ease:Power2.easeIn});
});

$(".survey a").click(function() {
	back();

	notification.show();
  var tl = new TimelineLite();
  tl.to(notification,0.25,{css:{opacity:1}, ease:Power2.easeIn})
  	.to(notification,3.25,{css:{opacity:0}, ease:Power2.easeIn});
  setTimeout(function() {
  	notification.hide();
  },3500);
});
